const jwt = require("jsonwebtoken");
const secret = "CrushAkoNgCrushKo";

//JSON Web Token or JWT is a way of securely passing information from the server to the front end or to other parts of server
//Information is kept secure through the use of secret code
//Only the system that knows the secret code that can decode the encrypted information


//Token Creation
/*
- Analogy
	Pack the gift and provide a lock with the secret code as the key
*/

module.exports.createAccessToken = (user) => {
	//The data will be received from the registration form
	//When the user logs in, a token will be created with user's information
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};


	//Generate a JSON web token using the jwt's method (sign())

	return jwt.sign(data, secret, {})
}


//token verification

module.exports.verify = (req, res, next) => {

	let token = req.headers.authorization;
	
	

	if (typeof token !== "undefined") {

		console.log(token);

		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {
			if (err) {
				return res.send({auth: "failed"});
			}else {

				next();
			}
		})
	} else {
		return res.send({auth: "token undefined"})
	}

}

module.exports.decode = (token) => {

	//Token received and is not undefined
	if(typeof token !== "undefined"){

		//Retrieves only the token and removes the "Bearer " prefix
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err, data) => {
			if(err) {
				return null
			} else {
				return jwt.decode(token, { complete:true }).payload;
			}
		})

	}else {
		//token does not exist
		return null
	}

}











