const express = require("express");
const router = express.Router();

const auth = require("../auth");

const courseController = require("../controllers/coursecontroller");



router.post("/", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	

	console.log(userData.isAdmin);

	if (userData.isAdmin) {

	courseController.addCourse(req.body).then(result => res.send(result));

}	else{

	res.send(false);
}


});



router.get("/all", (req, res) => {
	courseController.getAllCourses().then(result => res.send(result));
});


router.get("/", (req, res) => {
	courseController.getActiveCourses().then(result => res.send(result));
});


router.get("/:courseId", (req, res) => {
	console.log(req.params.courseId)
	courseController.getCourse(req.params.courseId).then(result => res.send(result));
})

router.put("/:courseId", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if (data.isAdmin) {
		courseController.updateCourse(req.params.courseId, req.body).then(result => res.send(result))
	} else {
		res.send(false);
	}
})



router.put("/:courseId/archive", auth.verify, (req, res) => {
	const isAdmin = auth.decode(req.headers.authorization).isAdmin;
	

	if (isAdmin) {
		courseController.archiveCourse(req.params.courseId).then(result => res.send(result))
	} else {
		res.send(false);
	}
})




module.exports = router;