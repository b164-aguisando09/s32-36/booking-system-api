const express = require('express');
const router = express.Router();

const auth = require("../auth");

const userController = require("../controllers/usercontroller");




router.post("/checkEmail", (req, res) => {
	console.log("hi");
	userController.checkEmailExists(req.body).then(result => res.send(result))
});


//registration user
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(result => res.send(result))
});

//User authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(result => res.send(result))
});


router.post("/details", (req, res) => {
	userController.getProfile(req.body).then(result => res.send(result))
})

//The "auth.verify" acts as a middleware to ensure that the user is logged in before they can get the details of a user
router.get("/details", auth.verify, (req, res) => {
	//decode() to retrieve the user information from the token passing the "token" from te request headers as an argument

	const userData = auth.decode(req.headers.authorization);
	

	userController.getProfile(userData).then(result => res.send(result))
} )

router.post("/enroll", auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	}

	userController.enroll(data).then(result => res.send(result));
})


module.exports = router;