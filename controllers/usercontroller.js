const User = require('../models/user');
const bcrypt = require('bcrypt');
const auth = require('../auth');
const Course = require('../models/course');

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if (result.length > 0) {
			return true;
		} else {
			return false;
		}
	})
}


module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		mobileNo: reqBody.mobileNo,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {
		if (error) {
			return false;

		} else{
			return true;
		}
	})
}

//User authentication

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if (result == null) {
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if (isPasswordCorrect) {
				return { accessToken: auth.createAccessToken(result.toObject()) }
			}else{
				return false;
			}
		}
	})
}


module.exports.getProfile = (reqBody) => {
	return User.findById(reqBody.id).then((result,error) => {
		if (error) {
			return false;
		} else{

			result.password = "";

			return result;
		}
	})
}

//enroll user to a course


//async and wait - allow the process to wait for each other

module.exports.enroll = async (data) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {

		user.enrollments.push({courseId: data.courseId});

		return user.save().then((user, error) => {
			if (error) {
				return false;
			} else{
				return true;
			}
		})
	});

	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		course.enrollees.push({userId: data.userId});

		return course.save().then((course, error) => {
			if (error) {
				return false;
			} else{
				return true;
			}
		})
	})

	if (isUserUpdated && isCourseUpdated) {
		return true;
	} else {
		return false;
	}
}

