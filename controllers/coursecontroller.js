const Course = require("../models/course");

module.exports.addCourse = (reqBody) => {

	console.log(reqBody);




//add course with validation of admin
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

	return newCourse.save().then((course, error) => {
		if (error) {
			return false;
		} else{
			return true;
		}
	})
}


//retrieve all courses
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
}

module.exports.getActiveCourses = () => {
	return Course.find({ isActive: true}).then(result => {
		return result;
	})
}

module.exports.getCourse = (courseId) => {
	return Course.findById(courseId).then(result => {
		return result;
	})
}

module.exports.updateCourse = (courseId, reqBody) => {

	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	return Course.findByIdAndUpdate(courseId, updatedCourse).then((course, error) => {
		if (error) {
			return false;

		} else{
			return true;
		}
	})
}


module.exports.archiveCourse = (courseId) => {

	let archive = {
		isActive: false
	}
	
	return Course.findByIdAndUpdate(courseId, archive).then((course, error) => {
		if (error) {
			return false;

		} else{
			return true;
		}
	})
}